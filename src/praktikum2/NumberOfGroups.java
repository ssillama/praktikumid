package praktikum2;

import lib.TextIO;

public class NumberOfGroups {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Insert the number of people");
		int numberOfPeople = TextIO.getlnInt();

		System.out.println("Insert the size of the group");
		int sizeOfTheGroup = TextIO.getlnInt();

		int numberOfGroups = numberOfPeople / sizeOfTheGroup;
		System.out.println("There can be " + numberOfGroups + " groups");

		int surplus = numberOfPeople % sizeOfTheGroup;
		System.out.println("The surplus of people is " + surplus);
	}

}
