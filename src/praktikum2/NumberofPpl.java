package praktikum2;

import lib.TextIO;

public class NumberofPpl {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TextIO.put("Please enter the number of people: ");
		int people = TextIO.getlnInt();
		TextIO.put("Please enter the size of group: ");
		int size = TextIO.getlnInt();
		TextIO.putln("There can be made " + people / size + " groups");
		TextIO.put("There will be " + (people % size) + " people left over" );

	}

}
