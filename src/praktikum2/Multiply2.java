package praktikum2;

import lib.TextIO;

public class Multiply2 {
	public static void main(String[] args) {
		
		System.out.println("Insert two numbers");
		System.out.println("The answer is : " + TextIO.getlnInt() * TextIO.getlnInt());
	}

}
