package praktikum2;

import lib.TextIO;

public class Multiply {
	public static void main(String[] args) {
		
		//muutujate deklaleerimine
		int number1;
		int number2;
		int multiply;
		
		System.out.println("Insert two numbers.");
		//muutujate väärtus
		number1 = TextIO.getlnInt();
		number2 = TextIO.getlnInt();
		
		multiply = number1 * number2;
		
		System.out.println("The answer is : " + multiply);
		
		
	

	}
}