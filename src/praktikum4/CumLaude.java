package praktikum4;

import lib.TextIO;

public class CumLaude {

	public static void main(String[] args) {

		System.out.println("Please enter the grade of thesis");
		int grade = TextIO.getlnInt();
		if (grade < 0 || grade > 5 ) {
			System.out.println("Error!");
		    return;
		}
		System.out.println("Please enter the mean grade");
		double mean = TextIO.getDouble();
		if (mean < 0 || mean > 5) {
			System.out.println("Error!");
			return;
		}


		if (mean > 4.5 &&  grade==5) {
			System.out.println("You will get cum laude!");
		} else {
			System.out.println("You will not get cum laude!");
		}

		}

	}

