package praktikum6;

import lib.TextIO;

public class Methods2 {

	public static void main(String[] args) {
		
		//int userPut = userInput(0, 10);
		//System.out.println("User input was: " + userPut);
		
		
		System.out.println("You entered " + userInput(0, 100));
		System.out.println(randomNumber(0, 2));
	}

	public static int userInput(int min, int max){
		while (true) {
			System.out.println("Please enter a number between " + min + " and " + max);
			int input = TextIO.getlnInt();
			if (input >= min || input <= max) {
				
				return input;
			} else {	
				System.out.println("Invalid input");

			}
			
				
			}
		}
	public static int randomNumber(int min, int max) {
		int vahemik = max - min +1;
		return (int) (min + (Math.random() * vahemik ));
		
	}

}

