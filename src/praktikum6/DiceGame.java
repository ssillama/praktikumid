package praktikum6;

public class DiceGame {

	public static void main(String[] args) {
		
		System.out.println("This program will toss 2 dices for you and 2 for computer.");
		System.out.println("Then it will add them and will return the winner.");
		int playerDice1 = (int) (Math.random()*6 +1);
		int playerDice2 = (int) (Math.random()*6 +1);
		int computerDice1 = (int) (Math.random()*6 +1);
		int computerDice2 = (int) (Math.random()*6 +1);
		
		int sum1 = playerDice1 + playerDice2;
		int sum2 = computerDice1 + computerDice2;
		
		
		if (sum1 > sum2) {
			System.out.println("You win, your result is " + sum1 + " vs computer's result " + sum2);
		} else {
			System.out.println("Computer wins with a result: " + sum2 + " vs your result: " + sum1);
		}
	}

}
