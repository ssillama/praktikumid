package praktikum6;

import lib.TextIO;

public class HeadsOrTails {

	public static void main(String[] args) {
		
		while (true) {
			
		
		System.out.println("Heads(0) or Tails(1)?");
		int input = TextIO.getInt();
		int toss = (int) (Math.random() *2);
		if (input == toss) {
			System.out.println("Correct!");
			
		} else {
			System.out.println("You guessed wrong");
		}
		}
	}

}