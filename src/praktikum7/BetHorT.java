package praktikum7;

import lib.TextIO;

public class BetHorT {

	public static void main(String[] args) {
		System.out.println("You will start with 100 money");
		System.out.println("Please enter your bet from 1 to 25");
		System.out.println("Bet step is 1");
		System.out.println("If the coin toss is Heads you will double your bet");
		System.out.println("Good luck");

		int money = 100;
		while (true) {
			int toss = (int) (Math.random() * 2);
			System.out.println("Your bet: ");
			int bet = TextIO.getlnInt();
			
			money = money - bet;
			
			if (bet < 1 || bet > 25) {
				System.out.println("Please enter a number between 1 and 25 ");
				money = money + bet;

			} else if (toss == 0) {
				money = money + bet * 2;
				System.out.println("Coin toss was Heads, you now have " + money + " money");

			} else {
				
				System.out.println("You lose your bet");
			}
			if (money == 0) {
				System.out.println("Good game");
				break;
			}
		}
	}

}
