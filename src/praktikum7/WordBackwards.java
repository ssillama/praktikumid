package praktikum7;

import lib.TextIO;

public class WordBackwards {
	
	public static void main(String[] args) {
		System.out.println("Please enter a word: ");
		String word = TextIO.getlnString();
		System.out.println(backwards(word));
		
	}
	public static String backwards(String forward) {
		String backwards = "";
		for (int i = forward.length() - 1; i >=0 ; i--) {
			backwards = backwards + forward.charAt(i);
		}
		return backwards;
	}
}
