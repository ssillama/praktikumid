package praktikum7;

import lib.TextIO;

public class GuessingGame {

	public static void main(String[] args) {
		System.out.println("The computer will think about a number from 1 to 100");
		System.out.println("Please guess the number: ");

		int number = (int) (Math.random() * 100 + 1);
		while (true) {
			int entry = TextIO.getlnInt();
			
			if (entry == number) {
				System.out.println("Correct");
				break;
			} else if (entry < number) {
				System.out.println("Entry was lower");
			} else {
				System.out.println("entry was higher");

			}
	
	}
}
}