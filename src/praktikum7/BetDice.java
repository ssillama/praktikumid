package praktikum7;

import lib.TextIO;

public class BetDice {

	public static void main(String[] args) {
		System.out.println("You will start with 100 money");
		System.out.println("Please enter your bet from  0 to your remaining money");
		System.out.println("Bet step is 1");
		System.out.println("Please choose a dice number from 1 to 6");
		System.out.println("If your guess is right you will win your bet x6");
		System.out.println("Good luck");

		int money = 100;
		while (true) {
			int toss = (int) (Math.random() * 6 + 1);
			System.out.println("Your choice is:");
			int choice = TextIO.getlnInt();
			System.out.println("Please enter your bet");
			int bet = TextIO.getlnInt();
			money = money - bet;

			if (bet < 0 || bet > money + bet) {
				System.out.println("Please enter a bet between 0 and your remaining money ");
				money = money + bet;
				System.out.println("You have " + money + " money remaining");

			} else if (toss == choice) {
				money = money + bet * 6;
				System.out.println("Your guess is right, you now have " + money + " money");

			} else {

				System.out.println("You lose your bet");
				System.out.println("You have " + money + " money remaining");
			}
			if (money == 0) {
				System.out.println("Good game");
				break;
			}
		}
	}

}
