package harjutamine;

import java.util.ArrayList;

public class ArraylistPractise {

	public static void main(String[] args) {
		ArrayList<String> koerad = new ArrayList<String>();
		koerad.add("Pontu");
		koerad.add("Lontu");
		koerad.add("Kontu");
		koerad.add("Rex");
		koerad.add("Muri");
		
		koerad.add(2, "Karvane");
		
		System.out.println("There are " + koerad.size() + " dogs");
	}

}
