package harjutamine;

import lib.TextIO;

public class HowToMakeATextFile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String name;
		String email;
		String salary;
		
		//What this program does
		
		TextIO.putln("This program will create");
		TextIO.putln("your profile file, if you just");
		TextIO.putln("will answer the questions.");
		
		// Gathering data
		
		TextIO.put("What is your name? ");
		name = TextIO.getln();
		TextIO.put("What is your email? ");
		email = TextIO.getln();
		TextIO.put("What is your salary? ");
		salary = TextIO.getln();
		
		// Write the info in text file named info.txt
		
		TextIO.writeFile("info.txt");
		TextIO.putln("Users name is : " + name);
		TextIO.putln("Users email is : " + email);
		TextIO.putln("Users salary is : " + salary);
		
		// Print out final statement
		
		TextIO.writeStandardOutput();
		TextIO.put("Thank you for giving us your information!");
	}

}
