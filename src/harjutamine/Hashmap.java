package harjutamine;

import java.util.HashMap;

public class Hashmap {

	public static void main(String[] args) {
		HashMap<Integer, String> playersInGame = new HashMap<Integer, String>();
		playersInGame.put(2, "Counter Strike");
		playersInGame.put(4, "Tetris");
		playersInGame.put(3, "Pinball");
		playersInGame.put(2, "Mass Effect 3");
		playersInGame.put(5, "Witcher 3");
		
		System.out.println("There are " + playersInGame.size() + " games");
			
		for (Integer a : playersInGame.keySet()) {
			System.out.println(a + " are playing " + playersInGame.get(a));
		}
		}
	}

