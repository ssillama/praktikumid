package harjutamine;

import lib.TextIO;

public class Interestt {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TextIO.put("Enter initial investment : ");
		double principal = TextIO.getlnDouble();
		TextIO.put("Enter the annual investment rate :");
		double rate = TextIO.getDouble();
		
		double interest = principal * rate;
		double value = principal + interest;
		TextIO.putln("The amount of interest is : $" + interest);
		TextIO.put("The value after one year is : $" + value);
	}

}
