package harjutamine;

import java.util.HashMap;

public class HahsMapPractise {

	public static void main(String[] args) {
		HashMap<String, Integer> studentsAndAge = new HashMap<String, Integer>();
		studentsAndAge.put("Paula", 21);
		studentsAndAge.put("Joel", 23);
		studentsAndAge.put("Hanna", 22);
		studentsAndAge.put("Mari", 20);
		studentsAndAge.put("Helen", 22);
		for (String name : studentsAndAge.keySet()) {
			System.out.println(name + " is aged " + studentsAndAge.get(name));
		}
		}

	}


