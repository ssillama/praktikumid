package harjutamine;

import lib.TextIO;

	public class Answer3 {
	    public static int numberOfItems(int [] m) {
	        // Your code here!
	        return m.length;
	    }

	    public static void main(String [] args) {
	        int [] test = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	        System.out.println( numberOfItems(test) );
	    }
	}
