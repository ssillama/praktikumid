package harjutamine;

import lib.TextIO;

public class Ruutv6rrand1 {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		TextIO.putln("Sisesta ruutv6rrandi kordajad : ");
		TextIO.put ("a = ");
		double a = TextIO.getDouble();
		TextIO.put ("b = ");
		double b = TextIO.getDouble();
		TextIO.put ("c = ");
		double c = TextIO.getDouble();
		
		if (Math.abs(a) < 000000.1)
			throw new Exception ("\nTegemist pole ruutv6rrandiga");
		double d = (b*b - 4*a*c);
		if (d < 0)
			throw new Exception ("\nReaallahendid puuduvad");
		double x1 = (-b + Math.sqrt(d) / 2*a);
		double x2 = (-b - Math.sqrt(d) / 2*a);
		TextIO.putln("x1 = " + x1);
		TextIO.putln("x2 = " + x2);	
		
	}

}
