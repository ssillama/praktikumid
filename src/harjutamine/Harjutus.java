package harjutamine;

import lib.TextIO;

public class Harjutus {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double principal;
		double rate;
		double interest;

		TextIO.put("Please enter principal : ");
		principal = TextIO.getDouble();
		TextIO.put("Please enter the annual rate : ");
		rate = TextIO.getDouble();

		interest = principal * rate;
		principal = principal + interest;

		TextIO.putln("The annual interest is : " + interest);
		TextIO.put("The new principal is : " + principal);

	}

}
