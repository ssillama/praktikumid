package praktikum12;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Gradient1 extends Applet {
	
	@Override
	public void paint(Graphics g) {
		// TODO Auto-generated method stub
		int w = getWidth();
		int h = getHeight();
		
		for (int y = 0; y < h; y++) {
			
			double concentrate = (double) y / h;
			concentrate = 1 - concentrate;
			int juice =  (int) (concentrate * 255);
			Color color = new Color (juice, juice, 255);
			g.setColor(color);
			
			
			g.drawLine(0, y, w, y);
			
			
		}

	}

}
