package praktikum9;

import java.util.ArrayList;

import lib.TextIO;

public class Suuredt2hed {

	public static void main(String[] args) {
		System.out.println("Palun sisesta sõna");
		String s6na = TextIO.getlnString();
		tryki(s6na);
	}
	
	public static void tryki(String sisend) {
		for (int i = 0; i < sisend.length(); i++) {
			System.out.print(sisend.toUpperCase().charAt(i));
			if (i < sisend.length() - 1) {
				 System.out.print("-");
			}
		}
	}

}