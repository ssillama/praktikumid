package praktikum9;

import java.util.ArrayList;

import lib.TextIO;

public class KysiNimi {

	public static void main(String[] args) {
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		System.out.println("Sisesta nimi:");
		String nimi1 = TextIO.getlnString();
		System.out.println("Sisenda vanus:");
		int a = TextIO.getlnInt();
		inimesed.add(new Inimene(nimi1, a));

		for (Inimene inimene : inimesed) {
		    // Java kutsub välja Inimene klassi toString() meetodi
		    System.out.println(inimene);
		    
		}

	}

}
